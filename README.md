# Local Override for Node Sass imports

> Allows you to override `@import`-ed files from vendor by placing them
> at correct path in local directory

To make it easier to undestand imagine a following example:

You have your SCSS that imports another SCSS from vendor library.

**your-style.scss**
```scss
@import 'path/to/vendor/style';
```

**vendor/main.scss**
```scss
@import './functions';
@import './variables';
@import './page/gallery';

// ... more scss
```

In this example if you want to override the page/gallery style, you'd have to
clone vendor/main.scss, and update paths to local ones for styles you want to
override.

By using this importer you'll be able to tell node-sass to check first your
folder for file, and then if local doesn't exist, fallback to original.

So loading for function will work by checking and loading file from first existing
path:

1. local/functions.scss
2. path/to/vendor/functions.scss

## Installation

`npm install node-sass-importer-local-override`

## Usage

According to node-sass documentation (https://github.com/sass/node-sass#importer--v200---experimental)
add the importer function to node-sass options

```js
const sass = require('node-sass');
const localOverride = require('node-sass-importer-local-override');

const vendorFolder = path.resolve('./vendor/package')
const localFolder = path.resolve('./src/package-overrides')

var result = sass.renderSync({
  data: scss_content
  importer: localOverride(vendorFolder, localFolder)
});
```
