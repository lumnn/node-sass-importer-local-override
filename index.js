const path = require('path')
const sassResolve = require('@csstools/sass-import-resolve')

/**
 * Creates a sass importer that checks if any style from
 * vendor/snowdog/theme-blank-sass tries to import other file. Importer checks
 * first if the requested file is local theme. If yes then it uses the local
 * file instead of default.
 *
 * @param  {String}   projectFolder The project folder
 * @param  {String}   vendorFolder  The vendor folder
 * @return {Function} sass importer function
 */
module.exports = function sassImporterLocalOverrideFactory (vendorFolder, overrideFolder) {
  return function sassImporterLocalOverride (url, prev, done) {
    if (!prev || !prev.includes(vendorFolder)) {
      return null
    }

    var context = path.dirname(prev)
    context = context.replace(vendorFolder + path.sep, '')

    sassResolve(path.resolve(overrideFolder, context, url))
      .then(resolved => {
        console.log('local override: ' + url + ' from ' + context)
        done({ file: resolved.file })
      })
      .catch(e => {
        done(null)
      })
  }
}
